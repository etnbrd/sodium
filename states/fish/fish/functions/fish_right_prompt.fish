function fish_right_prompt
  set -l status_test $status
  set -l status_glyph
  set -l duration

  set -l color_normal      (set_color normal)
  set -l color_info        (set_color white)
  set -l color_dim         (set_color blue)
  set -l color_back        (set_color -d blue)
  set -l color_error       (set_color red)
  set -l color_dirty       (set_color red)
  set -l color_staged      (set_color yellow)
  set -l color_clean       (set_color green)

  set -l color_green       (set_color -o green)
  set -l color_ice         (set_color -o blue)

  set -l git_summary

  set -l glyph_error "⚠"

  if test "$status_test" -ne 0
    set status_glyph $color_error $glyph_error " " $color_normal
  end

  if test "$CMD_DURATION" -gt 250
    set duration $color_dim (echo $CMD_DURATION | humanize_duration) " " $color_normal
  end


  if git_is_repo

    set -l git_stashed_glyph $color_ice "❄" $color_normal
    # set -l git_unmerged_glyph "⚡"
    set -l git_touched_glyph $color_green "±" $color_normal
    set -l git_status_glyph "•"
    set -l git_open $color_back "❲" $color_normal
    set -l git_close $color_back "❳" $color_normal
    set -l git_commit $color_dim (string sub -s 1 -l 3 (command git rev-parse HEAD)) $color_normal
    set -l git_branch $color_dim "⌥ " $color_info (git_branch_name) $color_normal
    set -l git_touched
    set -l git_stashed
    set -l git_remote

      # if git_is_staged
      #     if git_is_dirty
      #         set git_glyph "*"
      #     else
      #         set git_glyph "+"
      #     end

      if git_is_dirty
        set git_status $color_dirty $git_status_glyph $color_normal
      else if git_is_staged
        set git_status $color_staged $git_status_glyph $color_normal
      else
        set git_status $color_clean $git_status_glyph $color_normal
      end

      # if git_is_touched
      #   set git_touched $git_touched_glyph
      # end

      if git_is_stashed
        set git_stashed $git_stashed_glyph
      end

      set git_remote (git_ahead "↑" "↓" "↑↓")

      set git_summary \
        $git_open " "\
        $git_branch " "\
        $git_status " "\
        $git_commit " "\
        $git_stashed\
        $git_touched\
        $git_remote\
        $git_close;
  end

  echo -sn $duration $status_glyph $git_summary
end
