function fish_prompt
  set -l status_copy $status
  set -l user (id -un $USER)
  set -l shell_type
  set -l user_type

  set -l color_normal       (set_color normal)
  set -l color_dim          (set_color blue)
  set -l color_back         (set_color -o blue)
  set -l color_user         (set_color white)
  set -l color_root         (set_color red)
  set -l color_local        (set_color green)
  set -l color_container    (set_color yellow)
  set -l color_ssh          (set_color red)

  set -l shell_type_glyph   "❱"
  set -l user_type_glyph    "❱"
  set -l shell_glyph        $color_back "⚛ " $color_normal # ⚓

  # Change color according to shell environement (remote, container, or local)
  if test -n "$SSH_CLIENT" -o -n "$SSH_TTY"
    # On a remote shell
    set shell_type $color_ssh $shell_type_glyph $color_normal
  else if test -e /.dockerenv
    # Inside a docker container
    set shell_type $color_container $shell_type_glyph $color_normal
  else
    # Any other case
    set shell_type $color_local $shell_type_glyph $color_normal
  end

  # Change color according to user type
  switch "$USER"
    case root toor
      set user_type $color_root $user_type_glyph $color_normal
    case '*'
      set user_type $color_user $user_type_glyph $color_normal
  end

  echo -sn \
    $shell_glyph \
    $color_dim (prompt_pwd) " "\
    $shell_type\
    $user_type\
    $color_normal " ";
end
