{%- set user = pillar['users'].keys()[0] -%}

{%- if "host" in grains['roles'] -%}

docker:
  pkg.installed:
    - pkgs:
      - docker
  group.present:
    - members:
      - {{ user }}

rkt:
  pkg.installed:
    - pkgs:
      - rkt
      - acbuild

{%- endif -%}
