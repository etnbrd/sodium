{%- from 'macros/etcfile.jinja' import etcfile -%}
{%- from 'macros/git.jinja' import gitclone -%}
{%- set cellar_root = pillar['cellar_root'] -%}
{%- set salt_root = pillar['salt_root'] -%}
{%- set user = pillar['users'].keys()[0] -%}

base.packages:
  pkg.installed:
    - pkgs:
      # Kernel
      - linux-headers
      - dkms
      # Base tools
      - sudo
      - which
      - grep
      - zsh
      - vim
      - git
      - rsync
      - net-tools
      - bind-tools
      # Fs
      - btrfs-progs
      - hfsprogs
      - ntfs-3g
      # Salt
      - salt
      # SSH
      - openssh
      - sshfs
      - mosh
      # Monitor
      - procps-ng
      - htop
      - iotop
      - powertop
      - bmon

base.update:
  pkg.uptodate:
    - refresh: True

{{ gitclone(user, 'git@gitlab.com:etnbrd/chloride.git', salt_root + '/chloride') }}
{{ gitclone(user, 'git@gitlab.com:etnbrd/sodium.git', salt_root + '/sodium') }}
{{ gitclone(user, 'git@gitlab.com:etnbrd/settler.git', cellar_root + '/settler') }}

{{ etcfile(user, 'salt/minion', 'base/minion') }}
