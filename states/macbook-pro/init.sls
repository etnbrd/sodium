{%- from 'macros/aur.jinja' import aur -%}
{%- set sodium_root = pillar['sodium_root'] -%}
{%- set states_root = pillar['states_root'] -%}

# refind-efi: pkg.installed
{%- if grains['hardware'] is defined and "macbook-pro" in grains['hardware'] -%}

# TODO run only if refind-efi updated
refind:
  pkg.installed:
    - pkgs:
      - refind-efi
  cmd.run:
    - name: refind-install --usedefault /dev/disk/by-label/EFI --alldrivers
    - onchanges:
      - pkg: refind
    - require:
      - pkg: refind

{{ aur('broadcom-wl-dkms') }}

evdev-patch:
  file.patch:
    - name: /usr/share/X11/xkb/keycodes/evdev
    - source: {{ states_root }}/macbook-pro/evdev.patch
    - hash: md5=fb9765216325d438f4aedc800e18afc1
    - require:
      - pkg: gnome

{%- endif -%}
