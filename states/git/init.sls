{%- from 'macros/dotfile.jinja' import dotfile -%}
{%- set user = pillar['users'].keys()[0] -%}

{{ dotfile(user, '.gitconfig', 'git/.gitconfig') }}
{{ dotfile(user, '.gitignore', 'git/.gitignore') }}
