# Create profile
firefox -CreateProfile "JoelUser c:\internet\joelusers-moz-profile"

# In the firefox profile, these are the important files

### Template config (dotfiles)
places.sqlite # Bookmarks, Downloads and Browsing History
logins.json # Passwords
key3.db # Passwords encryption key
permissions.sqlite # Site-specific preferences
search.json.mozlz4 # Search engines
persdict.dat # Personal dictionary
mimeTypes.rdf # File types and download actions
addons.json # Addons
containers.json # Description of containers (work, bank, shopping ...)
content-prefs.sqlite # Individual settings for pages
prefs.js # All preferences
times.json # Creation and resets dates
extensions.json # Stores XPIProvider data previously stored in extensions.sqlite.

### Data (not config files)
xulstore.json # Toolbar and window size/position settings
sessionstore.js # Stored session
cert8.db # Security certificate settings
webappsstore.sqlite # DOM storage
formhistory.sqlite # Autocomplete history
cookies.sqlite # Cookies
storage # ???
storage.sqlite # ???

### Not backed up
gmp # ???
crashes # Not relevant for backup
minidumps # Not relevant for bakcup
saved-telemetry-pings # ot relevant for backup
datareporting # Not relevant for bakcup
SecurityPreloadState.txt # ???
Telemetry.ShutdownTime.txt # ???
sessionstore-backups # Session store backup, not relevant for bakcup, session is already stored in sessionstore.js
bookmarkbackups # Bookmarks backup, not relevant for backup -> bookmarks are in places.sqlite
secmod.db # Security module database, probably don't contains personal information
SiteSecurityServiceState.txt # Security stuffs, contains personal data, but probably not required for backups
sessionCheckpoints.json # probably not required for backups
blocklist.xml # Automatically generated, contains a list of potentialy harmful addons
compatibility.ini # Automatically generated
extensions.ini # Automatically generated
pluginreg.dat # Automatically generated
AlternateServices.txt # Empty or non existent
