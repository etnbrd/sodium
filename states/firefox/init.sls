{%- from 'macros/aur.jinja' import aur -%}
{%- from 'macros/credentials.jinja' import credentials -%}
{%- set chloride_root = pillar['chloride_root'] -%}
{%- set user = pillar['users'].keys()[0] -%}

{%- if "desktop" in grains['roles'] -%}

{{ aur('firefox-nightly') }}
{{ credentials('/home/' + user + '/.mozilla/firefox', 'firefox', user, '0600') }}

{# firefox:
  cmd.script:
    - name: {{ chloride_root }}/scripts/aur.sh
    - args: firefox-nightly #}

      {# pkg.installed:
        - pkgs:
          - dbus-glib
          - gtk3
          - libxt
          - mime-types
          - nss
          - ffmpeg
          - hunspell
          - hyphen
          - libnotify
          - speech-dispatcher #}

{%- endif -%}
