{%- set states_root = pillar['states_root'] -%}

sudoers-wheel:
  file.managed:
    - name: /etc/sudoers.d/wheel
    - source: {{ states_root + '/sudoers/wheel' }}
    - user: root
    - group: root
    - mode: 440
