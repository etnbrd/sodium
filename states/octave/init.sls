{%- from 'macros/dotfile.jinja' import dotfile -%}
{%- set user = pillar['users'].keys()[0] -%}

{%- if "octave" in grains['roles'] -%}

{{ dotfile(user, '.octavrc', 'octave/octaverc') }}

{%- endif -%}
