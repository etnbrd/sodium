{%- from 'macros/dotfile.jinja' import dotfile -%}
{%- from 'macros/git.jinja' import gitclone -%}
{%- set states_root = pillar['states_root'] -%}
{%- set user = pillar['users'].keys()[0] -%}

{{ dotfile(user, '.zshrc', 'zsh/zshrc') }}
{{ dotfile(user, '.zsh', 'zsh/zdotdir') }}
{{ gitclone(user, 'git@github.com:sorin-ionescu/prezto.git', states_root + '/zsh/zdotdir/prezto') }}
{{ dotfile(user, '.zsh/prezto/modules/prompt/functions/prompt_etnbrd_setup', 'zsh/prompt.zsh') }}
