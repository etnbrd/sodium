{%- from 'macros/pip.jinja' import pip -%}

{%- if "ml" in grains['roles'] -%}

{{ pip(['matplotlib', 'scipy', 'numpy', 'sklearn', 'tensorflow', 'theano']) }}

{%- endif -%}
