{%- from 'macros/aur.jinja' import aur -%}
{%- from 'macros/dotfile.jinja' import dotfile -%}
{%- set sodium_root = pillar['sodium_root'] -%}
{%- set user = pillar['users'].keys()[0] -%}

{%- if "desktop" in grains['roles'] -%}

{{ aur('atom-editor-beta-bin') }}

{% set atom_packages = [
  'atom-beautify',
  'atom-jade',
  'atom-jinja2',
  'firewatch-syntax',
  'language-javascript-jsx',
  'language-latex',
  'language-matlab',
  'language-docker',
  'language-pug',
  'multi-cursor-increment',
  'linter',
  'linter-ui-default',
  'glacier-syntax',
  'glacier-ui',
  'pigments',
] %}

atom-packages:
  pkg.installed:
    - pkgs:
      - libxss
  cmd.run:
    # TODO apm update don't install package, while apm install reinstall packages.
    # We need to install packages that are not already, and update packages that are already installed
    - name: /usr/share/atom-beta/resources/app/apm/bin/apm install {{ atom_packages|join(" ") }}
    - runas: {{ user }}
    - require:
      - cmd: atom-editor-beta-bin

{{ dotfile(user, '.atom/config.cson', 'atom/config.cson') }}
{{ dotfile(user, '.atom/init.coffee', 'atom/init.coffee') }}
{{ dotfile(user, '.atom/keymap.cson', 'atom/keymap.cson') }}
{{ dotfile(user, '.atom/snippets.cson', 'atom/snippets.cson') }}
{{ dotfile(user, '.atom/styles.less', 'atom/styles.less') }}

{%- endif -%}
