{%- from 'macros/aur.jinja' import aur -%}
{%- from 'macros/dconf.jinja' import dconf -%}
{%- from 'macros/dotfile.jinja' import dotfile -%}
{%- set sodium_root = pillar['sodium_root'] -%}
{%- set states_root = pillar['states_root'] -%}
{%- set user = pillar['users'].keys()[0] -%}

{%- if "desktop" in grains['roles'] -%}

include:
  - formulas.gnome

{{ dotfile(user, '.config/gtk-3.0/gtk.css', 'gnome/gtk.css') }}

{{ dconf(user, 'gnome/terminal.dconf') }}
{{ dconf(user, 'gnome/inputs.dconf') }}
{{ dconf(user, 'gnome/shell.dconf') }}

# fonts
fonts:
  pkg.installed:
    - pkgs:
      - otf-fira-mono
      - otf-fira-sans
      - ttf-inconsolata

{# { aur('ttf-montserrat') } #}

# theme
themes:
  pkg.installed:
    - pkgs:
      - arc-gtk-theme

{{ aur('gnome-shell-extension-pixel-saver') }}

# maximus-two:
#   cmd.script:
#     - name: {{ sodium_root }}/aur.sh
#     - args: gnome-shell-extension-maximus-two-git

{%- endif -%}
