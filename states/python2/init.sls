{%- if "python2" in grains['roles'] -%}

python2:
  pkg.installed:
    - pkgs:
      - python2
      - python2-pip

{%- endif -%}
