{%- from 'macros/credentials.jinja' import inPlaceCredentials -%}
{%- set hostname = pillar['network'].hostname -%}

{%- if "desktop" in grains['roles'] -%}

hostname:
  file.managed:
    - name: /etc/hostname
    - contents: {{ hostname }}

local.IPv4:
  host.only:
    - name: 127.0.0.1
    - hostnames:
      - localhost
      - localhost.localdomain
      - {{ hostname }}

local.IPv6 :
  host.only:
    - name: ::1
    - hostnames:
      - localhost
      - localhost.localdomain
      - {{ hostname }}

include:
  - formulas.NetworkManager

{{ inPlaceCredentials('/etc/NetworkManager', 'system-connections') }}

{%- endif -%}
