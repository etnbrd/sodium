{%- from 'macros/git.jinja' import gitclone -%}
{%- set user = pillar['users'].keys()[0] -%}

# Books
{{ gitclone(user, 'git@gitlab.com:etnbrd/books.git', '/home/etn/Books') }}

# Document
{{ gitclone(user, 'git@gitlab.com:etnbrd/jrnl.git', '/home/etn/Documents/jrnl') }}
{{ gitclone(user, 'git@gitlab.com:etnbrd/resume.git', '/home/etn/Documents/resume') }}
{{ gitclone(user, 'git@gitlab.com:etnbrd/etnbrd.gitlab.io.git', '/home/etn/Documents/etnbrd.gitlab.io') }}

# Projects
{{ gitclone(user, 'git@gitlab.com:etnbrd/mapper.git', '/home/etn/Projects/mapper') }}
{{ gitclone(user, 'git@gitlab.com:etnbrd/slash.git', '/home/etn/Projects/slash') }}
