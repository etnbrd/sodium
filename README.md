# Cellar

Cellar is a set of scripts to backup and setup machines.
It is roughly composed of 4 parts :
- Credentials (a private repository)
- [Sodium](https://gitlab.com/etnbrd/sodium)
- [Chloride](https://gitlab.com/etnbrd/chloride)
- [Settler](https://gitlab.com/etnbrd/settler)

Cellar is like a dotfile manager but for whole machines, using [SaltStack](https://saltstack.com/) (Sodium + Chloride = Salt).

## The story

I used to have a dotfile repo, like everybody else, with clever symlinks. I even implemented a filter mechanism to deploy dotfiles differently on different machines.
At the same time, I use Salt to provision and maintain my personal machines (in a masterless manner).
Salt is way more powerful than what I could ever implement in my dotfiles scripts.
So I switched from bash scripts to salt states for my dotfiles.
Here is the result.

## Example

You can see [this script](https://gitlab.com/etnbrd/settler/blob/master/deploy.sh) for a complete example of deploying the state on a base machine.

## The components

### [Sodium](https;//gitlab.com/etnbrd/sodium)

**[Sodium](https://gitlab.com/etnbrd.com/sodium)** is the personalised part of salt. This repo hosts my Sodium. You should host yours.

It contains all my dotfiles, and more (like locals, network configuration, sudoers files and so on) needed to transform an emtpy machine into the one I am accustomed to. It requires *Chloride*, and a another repo containing my credentials.

### [Chloride](https://gitlab.com/etnbrd/chloride)

**[Chloride](https://gitlab.com/etnbrd/chloride)** is the common part of salt.
It contains macros and formulas to ease the defintion of Sodium.
It intends to grow with time, and usage.

## Composition

### Folder structure

```
home/etn/.cellar
  | credentials
  ` salt
    | chloride
    ` sodium
```

### Highstate

`salt-call --local state.highstate`

# Sodium

## How it works?

Refer to [Chloride](https://gitlab.com/etnbrd/chloride) for documentation on the macros and formulas.

### Modifying the sodium states

Most of the files are symlinked, so that it is possible to modify them in place (or if other programs to modify them, the changes will be kept).

However, some configurations needs to be done directly in this repo :
- sudoers
- user password
- dconf settings (gnome/prefs.sls, gnome-terminal.dconf)
- NetworkManager wifi settings

### Modifying the sodium machines

The states are applied on every machine depending on its roles, and hardware definitions.

These defintions are found for each machine in a YAML configuration file in the folder `machines`.
Each configuration file define the `roles` and the `hardware` grains to know when to apply the states.

All the states are included by default, so each state should be filtered by a statement like this :
```
... includes
{%- if "desktop" in grains['roles'] -%}
... states
{%- endif -%}
```
